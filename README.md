AWS Setup
=========
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create resources with CloudFormation.

This role will create CloudFormation Stacks with the following resources per stack:
* setup
  * IAM Roles
    * ScaleHookRole
  * SNS Topics
    * ScaleSNS
  * SQS Queues
    * ScaleSQS
    * ScaleSQSDLQ
* setup-s3 (when create_s3 True)
  * S3 Bucket <account_name>-<account_abbr>-private
  * S3 Bucket <account_name>-<account_abbr>-logs
  * S3 BucketPolicy allowing all ALB's to write into the logs bucket

When create_s3 is set to True, buckets will be created for logs and private resources.
The logs bucket will allow all AWS ELB accounts to PUT objects and thus can be used for ELBs to write their logs to.
The private bucket is being used by other ansible roles such as aws-lambda and ecs-puppet but can be used generally for safekeeping files that need to be shared (to specific resources) within the account itself.

In addition to the CloudFormation stack, cfninit scripts will be placed into the provided S3 bucket.
These scripts are used by roles like aws-bastion and aws-ecs-cluster to allow SSH logins using IAM users and SSH key registerd with IAM.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  
Pip 18.x or higher (Python 2.7)

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
None

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
--------------
```yaml
cloudformation_tags: {}
tag_prefix         : "mcf"
aws_setup_params   :
  debug              : False
  environment_abbr   : "{{ account_abbr }}"
  cfninit_bucket_name: "{{ account_name }}-private"
  create_s3          : True
```

Example Playbooks
-----------------
Rollout the aws-setup files with defaults
```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "dta"
    environment_abbr: "dta"
  roles:
    - "aws-setup"
```
Rollout aws-setup without creating S3 buckets but with custom tags added
```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix         : "mcf"
    aws_region         : "eu-west-1"
    owner              : "myself"
    account_name       : "my-dta"
    account_abbr       : "dta"
    environment_type   : "dta"
    environment_abbr   : "dta"
    cloudformation_tags:
      my:project_nr: 123
      my:user      : "{{ lookup('env','USER') }}"
    aws_setup_params:
      create_s3: False
  roles:
    - "aws-setup"
```

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
